use bevy::prelude::*;
use bevy_third_person_camera::*;
pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_camera);
    }
}

fn spawn_camera(mut commands: Commands) {
    let camera = (
        Camera3dBundle {
            transform: Transform::from_xyz(0., 4.0, 19.0).looking_at(Vec3::ZERO, Vec3::ZERO),
            ..default()
        },
        ThirdPersonCamera {
            mouse_sensitivity: 5.,
            mouse_orbit_button_enabled: true,
            mouse_orbit_button: MouseButton::Left,
            zoom: Zoom::new(5., 20.),
            ..default()
        },
    );
    commands.spawn(camera);
}
