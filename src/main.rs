use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_third_person_camera::*;
mod camera;
use camera::CameraPlugin;
mod player;
use player::PlayerPlugin;
mod world;
use world::WorldPlugin;
mod monster;
use monster::MonsterPlugin;
fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            WorldPlugin,
            PlayerPlugin,
            CameraPlugin,
            ThirdPersonCameraPlugin,
            MonsterPlugin,
            WorldInspectorPlugin::new(),
            FrameTimeDiagnosticsPlugin::default(),
            LogDiagnosticsPlugin::default(),
        ))
        .add_systems(Update, bevy::window::close_on_esc)
        .run()
}
