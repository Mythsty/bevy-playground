use bevy::prelude::*;

pub struct MonsterPlugin;

impl Plugin for MonsterPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_many_monsters)
            .add_systems(Update, (look_at_player,));
    }
}

use crate::player::Player;

#[derive(Component)]
struct Monster;

fn look_at_player(
    mut set: ParamSet<(
        Query<&Transform, With<Player>>,
        Query<&mut Transform, With<Monster>>,
    )>,
) {
    let player_t = set.p0().single().translation;
    // let player = player_q.single();
    // let player_t = player.translation;

    for mut monster_t in set.p1().iter_mut() {
        monster_t.look_at(player_t, Vec3::Y);
    }
}

fn spawn_many_monsters(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    spawn_monster(&mut commands, &mut meshes, &mut materials, (0., 0., -10.));
}

fn spawn_monster(
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<StandardMaterial>>,
    xyz: (f32, f32, f32),
) {
    let mut create_cube =
        |size: f32, color: Color, xyz: (f32, f32, f32), name: &str| -> (PbrBundle, Name) {
            (
                PbrBundle {
                    mesh: meshes.add(Mesh::from(shape::Cube::new(size))),
                    material: materials.add(color.into()),
                    // transform: Transform::from_xyz(xyz.0, xyz.1, xyz.2),
                    transform: Transform {
                        translation: Vec3::new(xyz.0, xyz.1, xyz.2),
                        scale: Vec3::new(0.5, 0.5, 0.5),
                        ..default()
                    },
                    ..default()
                },
                Name::from(name),
            )
        };
    let monster_scale = 2.;
    let mut blue_cube = commands.spawn((
        create_cube(
            monster_scale,
            Color::BLUE,
            (xyz.0, monster_scale / 2., xyz.2),
            "Blue cube",
        ),
        Monster,
    ));
    blue_cube.with_children(|parent| {
        parent.spawn(create_cube(
            monster_scale / 2.,
            Color::RED,
            (0., 0., -monster_scale / 2.),
            "Red cube",
        ));
    });
}
