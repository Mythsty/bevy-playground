use bevy::prelude::*;
use bevy_third_person_camera::*;
pub struct PlayerPlugin;
impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_player)
            .add_systems(Update, player_movement);
    }
}
#[derive(Component)]
pub struct Player;

#[derive(Component)]
struct Speed(f32);

fn player_movement(
    keys: Res<Input<KeyCode>>,
    time: Res<Time>,
    mut player_q: Query<(&mut Transform, &Speed), With<Player>>,
    camera_q: Query<&Transform, (With<ThirdPersonCamera>, Without<Player>)>,
) {
    for (mut p_trans, player_speed) in player_q.iter_mut() {
        let cam = match camera_q.get_single() {
            Ok(c) => c,
            Err(e) => Err(format!("Error retrieving camera {}", e)).unwrap(),
        };
        let mut direction = Vec3::ZERO;

        if keys.pressed(KeyCode::W) {
            direction += cam.forward();
        }
        if keys.pressed(KeyCode::R) {
            direction += cam.back();
        }
        if keys.pressed(KeyCode::A) {
            direction += cam.left();
        }
        if keys.pressed(KeyCode::S) {
            direction += cam.right();
        }
        direction.y = 0.;
        if keys.pressed(KeyCode::ShiftLeft) {
            direction += Vec3::Y;
        }
        if keys.pressed(KeyCode::ControlLeft) {
            direction += -Vec3::Y;
        }
        let movement = direction.normalize_or_zero() * player_speed.0 * time.delta_seconds();
        p_trans.translation += movement;
        direction.y = 0.;
        if direction.length_squared() > 0. {
            p_trans.look_to(direction, Vec3::ZERO);
        }
    }
}

fn spawn_player(
    mut commands: Commands,
    assets: Res<AssetServer>,
    // mut meshes: ResMut<Assets<Mesh>>,
    // mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let flashlight = (
        SpotLightBundle {
            spot_light: SpotLight {
                shadows_enabled: true,
                ..default()
            },
            transform: Transform::from_xyz(0., 0., -0.5),
            ..default()
        },
        Name::new("Flashlight"),
    );
    let player = (
        //     PbrBundle {
        //         mesh: meshes.add(shape::Box::default().into()),
        //         material: materials.add(Color::WHITE.into()),
        //         transform: Transform::from_rotation(Quat::from_rotation_z(std::f32::consts::PI / 2.)),
        //         ..default()
        //     },
        SceneBundle {
            scene: assets.load("Player.gltf#Scene0"),
            transform: Transform::from_xyz(0., 0.5, 0.),
            ..default()
        },
        ThirdPersonCameraTarget,
        Player,
        Speed(7.0),
        Name::new("Player"),
    );
    commands.spawn(player).with_children(|parent| {
        parent.spawn(flashlight);
    });
}
