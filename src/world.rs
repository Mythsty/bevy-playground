use bevy::prelude::*;
use bevy::render::mesh::Indices;
use bevy::render::mesh::PrimitiveTopology;
use bevy::render::mesh::VertexAttributeValues::*;
use rand::Rng;
// use bevy::render::mesh::ATTRIBUTE_POSITION;
pub struct WorldPlugin;

#[derive(Component)]
struct Grass;

impl Plugin for WorldPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ClearColor(Color::rgb(85. / 255., 206. / 255., 255. / 255.)))
            .add_systems(Startup, (spawn_light, spawn_floor, spawn_grass_plane))
            // .add_systems(Update, (move_grass));
        ;
    }
}

fn spawn_light(mut commands: Commands) {
    let light = (
        DirectionalLightBundle {
            directional_light: DirectionalLight {
                illuminance: 8000.,
                shadows_enabled: true,
                ..default()
            },
            transform: Transform::from_xyz(0., 100., 100.).looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        },
        Name::new("Sun"),
    );
    commands.spawn(light);
}

fn spawn_floor(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let floor = (
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Plane::from_size(150.0))),
            material: materials.add(Color::rgba(0.36, 0.21, 0.07, 1.00).into()),
            ..default()
        },
        Name::new("Floor"),
    );
    commands.spawn(floor);
}

fn spawn_grass_plane(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let grass_density = 0.3;
    let grass_scale = 0.15;
    let (plane_x, plane_z) = (25i16, 25i16);
    let mut offset = rand::thread_rng();
    for x in -plane_x..plane_x {
        for z in -plane_z..plane_z {
            let x = (x as f32 + offset.gen_range(0.0..=0.8)) * grass_density;
            let z = (z as f32 + offset.gen_range(0.0..=0.2)) * grass_density;
            let grass_transform = Transform::from_xyz(x, 0., z)
                .with_scale(Vec3::new(grass_scale, grass_scale, grass_scale))
                .with_rotation(Quat::from_rotation_y(offset.gen_range(-3.14..=3.14)));

            spawn_grass_blade(&mut commands, &mut meshes, &mut materials, grass_transform);
        }
    }
}

fn spawn_grass_blade(
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<StandardMaterial>>,
    grass_transform: Transform,
) {
    let grass_blade = PbrBundle {
        mesh: meshes.add(create_grass_mesh()),
        material: materials.add(StandardMaterial {
            base_color: Color::DARK_GREEN,
            double_sided: true,
            cull_mode: None,
            ..default()
        }),
        transform: grass_transform,
        ..default()
    };
    commands.spawn(grass_blade).insert(Grass);
}
fn create_grass_mesh() -> Mesh {
    // Create a new mesh using a triangle list topology, where each set of 3 vertices composes a triangle.
    Mesh::new(PrimitiveTopology::TriangleStrip)
        // Add 4 vertices, each with its own position attribute (coordinate in
        // 3D space), for each of the corners of the parallelogram.
        .with_inserted_attribute(
            Mesh::ATTRIBUTE_POSITION,
            vec![
                [0.0, 0.0, 0.0],
                [1.0, 0.0, 0.0],
                [0.25, 2.0, 0.0],
                [0.75, 2.0, 0.0],
                [0.5, 3.0, 0.0],
            ],
        )
        // Assign a UV coordinate to each vertex.
        // .with_inserted_attribute(
        //     Mesh::ATTRIBUTE_UV_0,
        //     vec![[0.0, 1.0], [0.5, 0.0], [1.0, 0.0], [0.5, 1.0]],
        // )
        // Assign normals (everything points outwards)
        .with_inserted_attribute(
            Mesh::ATTRIBUTE_NORMAL,
            vec![
                [0.0, 0.0, 1.0],
                [0.0, 0.0, 1.0],
                [0.0, 0.0, 1.0],
                [0.0, 0.0, 1.0],
                [0.0, 0.0, 1.0],
            ],
        )
    // After defining all the vertices and their attributes, build each triangle using the
    // indices of the vertices that make it up in a counter-clockwise order.
    // .with_indices(Some(Indices::U32(vec![
    //     // First triangle
    //     0, 1, 3, // Second triangle
    //     1, 2, 3,
    // ])))
}
fn move_grass(
    mut grass_q: Query<&mut Handle<Mesh>, With<Grass>>,
    mut assets: ResMut<Assets<Mesh>>,
    time: Res<Time>,
) {
    for blade in grass_q.iter_mut() {
        if let Some(grass_mesh) = assets.get_mut(blade.id()) {
            let mut pupu = Vec::new();
            let dudu = match grass_mesh.attribute_mut(Mesh::ATTRIBUTE_POSITION).unwrap() {
                Float32x3(vertices) => vertices,
                _ => &mut pupu,
            };
            dudu[3][2] = f32::sin(time.elapsed_seconds());
        }
    }
}
